package no.accelerate.chinookjdbc.dataaccess.models;

public record CustomerGenre(int count_genre_id, int genre_id, String name) {
}
