package no.accelerate.chinookjdbc.dataaccess.models;

public record CustomerCountry(int totalCustomers, String country) {
}
