package no.accelerate.chinookjdbc.dataaccess.repository.customer;

import no.accelerate.chinookjdbc.dataaccess.models.Customer;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerCountry;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerGenre;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerSpender;
import no.accelerate.chinookjdbc.dataaccess.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    List<Customer> getCustomersByName(String firstName, String lastName);
    List<Customer> getCustomersPageLimit(Integer limit, Integer offset);
    CustomerCountry mostPopularCountry();
    CustomerSpender highestSpender();
    List<CustomerGenre> mostPopularGenreForCustomer(int id);
}
