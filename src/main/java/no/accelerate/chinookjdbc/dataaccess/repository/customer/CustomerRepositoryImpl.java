package no.accelerate.chinookjdbc.dataaccess.repository.customer;

import no.accelerate.chinookjdbc.dataaccess.models.Customer;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerCountry;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerGenre;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final Connection connection;

    public CustomerRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

/*
    public List<Customer> findAll() {
        return null;
    }
*/
    @Override
    public List<Customer> findAll(){
        Customer customer;
        List<Customer> list = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT customer_id, " +
                        "first_name, " +
                        "last_name, " +
                        "country, " +
                        "postal_code, " +
                        "phone, " +
                        "email " +
                        "FROM customer")) {
            ResultSet resultSet = statement.executeQuery();
            boolean hasResult;
            while (hasResult = resultSet.next()) {
                if (!hasResult) {
                    System.out.println("no data");
                } else {
                    customer = new Customer(
                            resultSet.getInt("customer_id"),
                            resultSet.getString("first_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("country"),
                            resultSet.getString("postal_code"),
                            resultSet.getString("phone"),
                            resultSet.getString("email")
                    );
                    list.add(customer);
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        //throw new SQLException("CustomerReadAll failed!!");
        return list;
    }

    @Override
    public Customer findById(Integer integer) {
        Customer customer = null;
        try (PreparedStatement statement = connection.prepareStatement("SELECT customer_id, " +
                "first_name, " +
                "last_name, " +
                "country, " +
                "postal_code, " +
                "phone, " +
                "email " +
                "FROM customer WHERE customer_id = ? ")) {
            statement.setInt(1, integer);
            ResultSet resultSet = statement.executeQuery();
            boolean hasResult;
            hasResult = resultSet.next();
            if (!hasResult) {
                System.out.println("no data");
            } else {
                //int id, String first_name,String last_name, String country, String postal_code, String phone, String email
                customer = new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"), resultSet.getString("last_name"), resultSet.getString("country"), resultSet.getString("postal_code"), resultSet.getString("phone"), resultSet.getString("email"));
//                return customer;
            }

        } catch(SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    //getCustomersByName
    @Override
    public List<Customer> getCustomersByName(String firstName, String lastName) {
        Customer customer;
        List<Customer> list = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT customer_id, " +
                        "first_name, " +
                        "last_name, " +
                        "country, " +
                        "postal_code, " +
                        "phone, " +
                        "email " +
                        "FROM customer " +
                        "WHERE ( " +
                        "        LOWER(first_name) LIKE LOWER('%" +  firstName+ "%') " +
                        "        OR LOWER(last_name) LIKE LOWER('%" + lastName + "%') " +
                        " )")) {
            ResultSet resultSet = statement.executeQuery();
            boolean hasResult;
            while (hasResult = resultSet.next()) {
                if (!hasResult) {
                    System.out.println("no data");
                } else {
//                    System.out.println(resultSet.getString("last_name"));
                    customer = new Customer(
                            resultSet.getInt("customer_id"),
                            resultSet.getString("first_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("country"),
                            resultSet.getString("postal_code"),
                            resultSet.getString("phone"),
                            resultSet.getString("email")
                    );
                    list.add(customer);
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        //throw new SQLException("CustomerReadAll failed!!");
        return list;
    }


    @Override
    public List<Customer> getCustomersPageLimit(Integer limit, Integer offset) {
        Customer customer;
        ArrayList<Customer> list = new ArrayList<>();
        try(PreparedStatement statement = connection.prepareStatement(
                "SELECT customer_id, " +
                        "first_name, " +
                        "last_name, " +
                        "country, " +
                        "postal_code, " +
                        "phone, " +
                        "email " +
                        "FROM customer " +
                        "ORDER BY customer_id " +
                        "LIMIT ? " +
                        "OFFSET ? ")) {
        statement.setInt(1,limit);
        statement.setInt(2,offset);
        ResultSet resultSet = statement.executeQuery();
            boolean hasResult;
            while (hasResult = resultSet.next()) {
                if (!hasResult) {
                    System.out.println("no data");
                } else {
//                    System.out.println(resultSet.getString("last_name"));
                    customer = new Customer(
                            resultSet.getInt("customer_id"),
                            resultSet.getString("first_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("country"),
                            resultSet.getString("postal_code"),
                            resultSet.getString("phone"),
                            resultSet.getString("email")
                    );
                    list.add(customer);
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


    @Override
    public int insert(Customer customer) {
        try(PreparedStatement statement = connection.prepareStatement("INSERT INTO customer (" +
                "customer_id, " +
                "first_name, " +
                "last_name, " +
                "country, " +
                "postal_code, " +
                "phone, " +
                "email) " +
                "VALUES ((select max(customer_id)+1 FROM customer), ?, ?, ?, ?, ?, ?)")) {
        statement.setString(1, customer.first_name());
        statement.setString(2, customer.last_name());
        statement.setString(3, customer.country());
        statement.setString(4, customer.postal_code());
        statement.setString(5, customer.phone());
        statement.setString(6, customer.email());
        statement.executeUpdate();
    } catch(SQLException e) {
        e.printStackTrace();
    }
        return 1;
}
    @Override
    public int update(Customer customer)  {
        try(PreparedStatement statement = connection.prepareStatement("UPDATE customer " +
                "SET first_name= ?, " +
                "last_name = ?, " +
                "country = ?, " +
                "postal_code = ?, " +
                "phone = ?, " +
                "email = ? " +
                "WHERE customer_id = ?")) {
        statement.setString(1, customer.first_name());
        statement.setString(2, customer.last_name());
        statement.setString(3, customer.country());
        statement.setString(4, customer.postal_code());
        statement.setString(5, customer.phone());
        statement.setString(6, customer.email());
        statement.setInt(7, customer.customer_id());
        statement.executeUpdate();
    } catch(SQLException e) {
        e.printStackTrace();
    }
        return 1;
}


    @Override
    public CustomerCountry mostPopularCountry() {
        CustomerCountry customerCountry1 = null;
        try(PreparedStatement statement = connection.prepareStatement("SELECT COUNT(country) as total_customers, " +
                "country " +
                "FROM customer " +
                "WHERE country = ( " +
                "SELECT country " +
                "FROM customer " +
                "GROUP BY country " +
                "ORDER BY COUNT(country) DESC, country " +
                "LIMIT 1" +
                ") " +
                " GROUP BY country ")) {
        ResultSet resultSet = statement.executeQuery();
            boolean hasResult;
            while (hasResult = resultSet.next()) {
                if (!hasResult) {
                    System.out.println("no data");
                } else {
                    customerCountry1 = new CustomerCountry(
                            resultSet.getInt("total_customers"),
                            resultSet.getString("country")
                    );
//                    customerCountry = resultSet.getString("country");
                }
            }

//            System.out.println("OK!");
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return customerCountry1;
    }

    @Override
    public CustomerSpender highestSpender() {
        CustomerSpender customerSpender = null;
        try {
            PreparedStatement statement2 = connection.prepareStatement("SELECT customer_id " +
                "FROM invoice " +
                "GROUP BY customer_id " +
                "ORDER BY SUM(total) DESC, customer_id " +
                "LIMIT 1 ");
            ResultSet resultSet2 = statement2.executeQuery();
            if (resultSet2.next()) {
                customerSpender = new CustomerSpender(resultSet2.getInt("customer_id"));
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }

    @Override
    public List<CustomerGenre> mostPopularGenreForCustomer(int id) {
        CustomerGenre customerGenre;
        int count = 0;
        List<CustomerGenre> genres = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(g.genre_id) as count_genre, g.genre_id, g.name " +
                "FROM customer c, invoice i, invoice_line il, track t, genre g " +
                "WHERE c.customer_id = i.customer_id " +
                "AND i.invoice_id = il.invoice_id " +
                "AND il.track_id = t.track_id " +
                "AND t.genre_id = g.genre_id " +
                "AND c.customer_id = ? " +
                "AND g.genre_id IN ( " +
                "SELECT g.genre_id " +
                "FROM customer c, invoice i, invoice_line il, track t, genre g " +
                "WHERE c.customer_id = i.customer_id " +
                "AND i.invoice_id = il.invoice_id " +
                "AND il.track_id = t.track_id " +
                "AND t.genre_id = g.genre_id " +
                "AND c.customer_id = ? " +
                "GROUP BY G.genre_id " +
                "ORDER BY COUNT(*) DESC " +
                ") " +
                "GROUP BY G.genre_id " +
                "order by COUNT(g.genre_id) DESC ");
        statement.setInt(1, id);
        statement.setInt(2, id);
        ResultSet resultSet = statement.executeQuery();
            boolean hasResult;
            while (hasResult = resultSet.next()) {
                if (!hasResult) {
                    System.out.println("no data");
                } else {
                    if (count==0) {
                        count = resultSet.getInt("count_genre");
                    }
                    if (count==resultSet.getInt("count_genre")) {
                        customerGenre = new CustomerGenre(
                                resultSet.getInt("count_genre"),
                                resultSet.getInt("genre_id"),
                                resultSet.getString("name")
                        );
                        genres.add(customerGenre);
                    }
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return genres;
    }


}

