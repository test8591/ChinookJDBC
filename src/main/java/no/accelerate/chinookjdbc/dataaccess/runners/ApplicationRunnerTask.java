package no.accelerate.chinookjdbc.dataaccess.runners;

import no.accelerate.chinookjdbc.dataaccess.models.Customer;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerCountry;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerGenre;
import no.accelerate.chinookjdbc.dataaccess.models.CustomerSpender;
import no.accelerate.chinookjdbc.dataaccess.repository.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ApplicationRunnerTask implements ApplicationRunner {

    private final CustomerRepository dao;

    public ApplicationRunnerTask(CustomerRepository dao) {
        this.dao = dao;
    }

    @Override
    public void run(ApplicationArguments args) throws SQLException {
        /* 1. Read all the customers in the database, this should display their: Id, first name, last name, country, postal code, phone number and email */
        List<Customer> list = dao.findAll();
        for(int i=0;i< list.size();i++) {
            System.out.println("1. AllCustomers: " + list.get(i) );
        }

        /*  2. Read a specific customer from the database (by Id), should display everything listed in the above point */
        Customer numberOneCustomer = dao.findById(12);
        System.out.println("\n2. OneCustomer: " + numberOneCustomer.toString());

        /* 3. Read a specific customer by name. HINT: LIKE keyword can help for partial matches. */
        String firstName = "Erik";
        String lastName = "Han";
        List<Customer> listCustomersByName = dao.getCustomersByName(firstName, lastName);
        for(int i=0;i< listCustomersByName.size();i++) {
            System.out.println("3. CustomersByName: " + listCustomersByName.get(i) );
        }

        /* 4. Return a page of customers from the database. This should take in limit and offset as parameters and make use
	      of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above
     	  should be reused.
        */
        int limit = 2;
        int offset = 4;
        List<Customer> listCustomersPageLimitOffset = dao.getCustomersPageLimit(limit, offset);
        for(int i=0;i< listCustomersPageLimitOffset.size();i++) {
            System.out.println("4. CustomersPageLimitOffset: " + listCustomersPageLimitOffset.get(i) );
        }

        /* 5. Add a new customer to the database. You also need to add only the fields listed above (our customer object) */
        Customer addCustomerDb = new Customer(0, "Kjetil", "Hansen", "Norway", "1476", "90914465", "test@test.com");
        dao.insert(addCustomerDb);
        System.out.println("5. addNewCustomer");

        /* 6. Update an existing customer. */
        Customer updateCustomer = new Customer(62, "Kjetil62", "62Hansen", "Norway", "1476", "90914465", "test@test.com");
        dao.update(updateCustomer);
        System.out.println("6. updateCustomer");

        /* 7. Return the country with the most customers. */
        CustomerCountry customerCountry = dao.mostPopularCountry();
        System.out.println("7. Country of most customers: " + customerCountry.toString());

        /* 8. Customer who is the highest spender (total in invoice table is the largest). */
        CustomerSpender customerSpenderId = dao.highestSpender();
        Integer customerId = customerSpenderId.id();
        Customer customerSpenderInfo = dao.findById(customerId);
        System.out.println("8. HighestCustomerSpender: " + customerSpenderInfo.toString());

        /* 9. For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context
              means the genre that corresponds to the most tracks from invoices associated to that customer.
        */
        List<CustomerGenre> customerGenreList = new ArrayList<>();
        customerGenreList = dao.mostPopularGenreForCustomer(12);
        for(int i=0;i< customerGenreList.size();i++) {
            System.out.println("9. MostPopularGenreForCustomer: " + customerGenreList.get(i) );
        }
    }

    @PreDestroy
    public void tearDown() {
        System.out.println("Goodbye");
    }
}
