package no.accelerate.chinookjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChinookJDBCApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChinookJDBCApplication.class, args);
    }

}
