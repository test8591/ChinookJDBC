
/*
1. Read all the customers in the database, this should display their: Id, first name, last name, country, postal code,
phone number and email.
*/
	SELECT customer_id, 
	first_name, 
	last_name, 
	country, 
	postal_code,
	phone,
	email
	FROM customer
/* 2. Read a specific customer from the database (by Id), should display everything listed in the above point. */
	SELECT customer_id, 
	first_name, 
	last_name, 
	country, 
	postal_code,
	phone,
	email
	FROM customer
	WHERE customer_id = ?
/* 3. Read a specific customer by name. HINT: LIKE keyword can help for partial matches. */
	SELECT customer_id, 
	first_name, 
	last_name, 
	country, 
	postal_code,
	phone,
	email
	FROM customer
	WHERE (
			LOWER(first_name) LIKE LOWER('%hel%') 
		   	OR LOWER(last_name) LIKE LOWER('%hel%') 
		)
/* 4. Return a page of customers from the database. This should take in limit and offset as parameters and make use 
	  of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above
 	  should be reused.
*/
	SELECT customer_id, 
	first_name, 
	last_name, 
	country, 
	postal_code,
	phone,
	email
	FROM customer
	ORDER BY customer_id
	LIMIT 4
	OFFSET 2
/* 5. Add a new customer to the database. You also need to add only the fields listed above (our customer object) */
	INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) VALUES ((select max(customer_id)+1 FROM customer), 'Erik', 'Hansen', 'Norway', '1087', '+47 90 91 44 35', 'erik-brandvold.hansen@telenor.no');
/* 6. Update an existing customer. */
	UPDATE customer
	SET first_name= '', 
	last_name = '', 
	country = '', 
	postal_code = '',
	phone = '',
	email = ''
	WHERE customer_id = ?
/* 7. Return the country with the most customers. */
SELECT COUNT(country) as total_customers, country
FROM customer
WHERE country = (
    SELECT country
    FROM customer
    GROUP BY country
    ORDER BY COUNT(country) DESC, country
    LIMIT 1
    )
GROUP BY country
/* 8. Customer who is the highest spender (total in invoice table is the largest). */
SELECT customer_id
FROM invoice
GROUP BY customer_id
ORDER BY SUM(total) DESC, customer_id
LIMIT 1
/* 9. For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context
 	  means the genre that corresponds to the most tracks from invoices associated to that customer. 
*/
/*
SELECT DISTINCT c.*
FROM customer c, invoice i, invoice_line il, track t, genre g
WHERE c.customer_id = i.customer_id
AND i.invoice_id = il.invoice_id
AND il.track_id = t.track_id
AND t.genre_id = g.genre_id
AND c.customer_id = 6

--RIKTIG kan bruker i Java
SELECT COUNT(g.genre_id), g.genre_id, g.name
FROM customer c, invoice i, invoice_line il, track t, genre g
WHERE c.customer_id = i.customer_id
AND i.invoice_id = il.invoice_id
AND il.track_id = t.track_id
AND t.genre_id = g.genre_id
AND c.customer_id = 6
GROUP BY G.genre_id
ORDER BY COUNT(G.genre_id) DESC
LIMIT 1
																										  
																										  
SELECT COUNT(g.genre_id), g.genre_id, g.name
FROM customer c, invoice i, invoice_line il, track t, genre g
WHERE c.customer_id = i.customer_id
AND i.invoice_id = il.invoice_id
AND il.track_id = t.track_id
AND t.genre_id = g.genre_id
AND c.customer_id = 6
GROUP BY G.genre_id
ORDER BY COUNT(*) DESC
*/
																										  
SELECT COUNT(g.genre_id) as count_genre, g.genre_id, g.name
FROM customer c, invoice i, invoice_line il, track t, genre g
WHERE c.customer_id = i.customer_id
AND i.invoice_id = il.invoice_id
AND il.track_id = t.track_id
AND t.genre_id = g.genre_id
AND c.customer_id = 12
AND g.genre_id IN (
	SELECT g.genre_id
	FROM customer c, invoice i, invoice_line il, track t, genre g
	WHERE c.customer_id = i.customer_id
	AND i.invoice_id = il.invoice_id
	AND il.track_id = t.track_id
	AND t.genre_id = g.genre_id
	AND c.customer_id = 12
	GROUP BY G.genre_id
	ORDER BY COUNT(*) DESC
)
GROUP BY G.genre_id
order by COUNT(g.genre_id) DESC
																	  


																										  
/* Not for use.. */
SELECT c.customer_id, COUNT(g.genre_id), g.genre_id
FROM customer c, invoice i, invoice_line il, track t, genre g
WHERE c.customer_id = i.customer_id
AND i.invoice_id = il.invoice_id
AND il.track_id = t.track_id
AND t.genre_id = g.genre_id
GROUP BY G.genre_id, g.genre_id, c.customer_id
ORDER BY c.customer_id, COUNT(G.genre_id) DESC, g.genre_id

																										  
																										  
SELECT COUNT(g.genre_id), g.genre_id, g.name
FROM customer c, invoice i, invoice_line il, track t, genre g
WHERE c.customer_id = i.customer_id
AND i.invoice_id = il.invoice_id
AND il.track_id = t.track_id
AND t.genre_id = g.genre_id
AND c.customer_id in (select customer_id from customer)
AND g.genre_id IN (
	SELECT g.genre_id
	FROM customer c, invoice i, invoice_line il, track t, genre g
	WHERE c.customer_id = i.customer_id
	AND i.invoice_id = il.invoice_id
	AND il.track_id = t.track_id
	AND t.genre_id = g.genre_id
	AND c.customer_id = 6
	GROUP BY G.genre_id
	ORDER BY COUNT(*) DESC
	LIMIT 1
)
GROUP BY G.genre_id
																										  
																										  