
-- 01_tableCreate.sql
CREATE TABLE Superhero (
	Id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	Name VARCHAR(100),
	Alias VARCHAR(20),
	Origin VARCHAR(20)
);


CREATE TABLE Assistant(
	Id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	Name VARCHAR(100)
);

CREATE TABLE Power(
	Id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	Name VARCHAR(100),
	Description VARCHAR(300)
);

-- PRIMARY KEYS